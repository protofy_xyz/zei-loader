// Module import
var fs = require('fs');
var Parser = require("binary-parser").Parser;

const zeiHeader = new Parser()
  .endianess("little")
  .uint8("code")
  .string("command", {length:3})
  .uint16("num1")
  .uint16("num2")
  .skip(6)
  .uint8("flag");

// Prepare buffer to parse.
fs.readFile('./Seat.zei', (err, buf) => {
    // Parse buffer and show result
    var data;
    var current=0;
    const instructions = [[]];
    while(buf.byteLength > zeiHeader.sizeOf()*current) {
        data = zeiHeader.parse(buf.slice(zeiHeader.sizeOf()*current));
        instructions.push(data);
        const signature = JSON.stringify(data);
        console.log((zeiHeader.sizeOf()*current).toString(16), data);
        current++;
    } 
    //console.log(instructions);
});
